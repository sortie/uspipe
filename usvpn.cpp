#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/epoll.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <endian.h>

int open_unix_socket_server(const char* uspath, bool unlink_if_exists)
{
	int sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if ( sock == -1 ) 
		return -1;

	sockaddr_un local;
	memset(&local, 0, sizeof(local));
	local.sun_family = AF_UNIX;
	if ( sizeof(local.sun_path) <= strlen(uspath) )
		return errno = ERANGE, -1;
	strcpy(local.sun_path, uspath);

	if ( unlink_if_exists )
		unlink(uspath);

	socklen_t localsize = SUN_LEN(&local);
	if ( bind(sock, (sockaddr*) &local, localsize) == -1 )
		return close(sock), -1;

	if ( listen(sock, 5) == -1 )
		return close(sock), -1;

	return sock;
}

struct Handshake // Big endian
{
	uint16_t handshakeversion;
	uint16_t minversion;
	uint16_t maxversion;
	uint16_t unused;
};

const uint16_t MSG_TO_CLIENT = 0x8000U;
const uint16_t MSG_EXIT = 0x0U;
const uint16_t MSG_CLOSECHANNEL = 0x1U;
const uint16_t MSG_NEWCHANNEL = 0x2U;
const uint16_t MSG_DATA = 0x3U;
const uint16_t MSG_ACK = 0x4U;
const uint16_t MSG_CONNECT = 0x5U;
const uint16_t MSG_DISCONNECT = 0x6U;
const uint16_t MSG_BIND = 0x7U;
const uint16_t MSG_LISTEN = 0x8U;
const uint16_t MSG_ACCEPT = 0x9U;

struct Message // Big endian
{
	uint16_t msg;
	uint16_t length;
	uint32_t channel;
};

struct MessageExit : public Message
{
};

struct MessageCloseChannel : public Message
{
};

struct MessageNewChannel : public Message
{
};

struct MessageReq : public Message
{
	uint8_t payload[];
};

struct MessageResp : public Message
{
	uint8_t payload[];
};

struct MessageConnect : public Message
{
	uint16_t protocoloff;
	uint16_t hostnameoff;
};


class EventHandler
{
public:
	virtual ~EventHandler() : canread(false), canwrite(false) { }
	virtual void OnEvent(struct epoll_event* event) { }

protected:
	virtual void OnError() { }
	virtual void OnHangUp() { }
	virtual void OnInput() { canread = true; }
	virtual void OnOutput() { canwrite = false; } 
	virtual void OnUrgent() { }

protected:
	bool canread, canwrite;

};

void EventHandler::OnEvent(struct epoll_event* event)
{
	if ( event->events & EPOLLERR )
		OnError(event);
	if ( event->events & EPOLLHUP )
		OnHangUp(event);
	if ( event->events & EPOLLIN )
		OnInput(event);
	if ( event->events & EPOLLOUT )
		OnOutput(event);
	if ( event->events & EPOLLPRI )
		OnUrgent(event);
}

class VPNConnection : public EventHandler
{
public:
	VPNConnection(int epfd, int fd);
	virtual ~VPNConnection();
	int FailedConstruction() const { return constructionfailnum; }

protected:
	virtual void OnError();
	virtual void OnHangUp();
	virtual void OnInput();
	virtual void OnOutput();

private:
	void OnInput(uint8_t* buf, size_t buflen);

private:
	int epfd;
	int fd;
	int constructionfailnum;

};

void VPNConnection::OnError()
{
}

void VPNConnection::OnHangUp()
{
}

void VPNConnection::OnInput()
{
	
}

void VPNConnection::OnOutput()
{
}

VPNConnection::VPNConnection(int epfd, int fd)
{
	this->epfd = epfd;
	this->fd = fd;

	struct epoll_event usevent;
	memset(&usevent, 0, sizeof(usevent));
	usevent.events = EPOLLERR | EPOLLHUP | EPOLLIN | EPOLLOUT | EPOLLPRI;
	usevent.data.ptr = this;
	if ( epoll_ctl(epfd, EPOLL_CTL_ADD, usfd, &usevent) )
		constructionfailnum = errno;
}
int main(int argc, char* argv[])
{
	int epfd = epoll_create(100);
	if ( epfd < 0 )
		error(1, errno, "epoll_create");

	const char* uspath = "sortix-serial";
	int usfd = open_unix_socket_server(uspath, true);
	if ( usfd < 0 )
		error(1, errno, "open_unix_socket_server: %s", uspath);
	
	size_t NUM_EVENTS = 16UL;
	struct epoll_event events[NUM_EVENTS];
	int num;
	while ( 0 < (num = retvalepoll_wait(epfd, events, (int) NUM_EVENTS, 0)) )
		for ( int i = 0; i < num; i++ )
			((EventHandler*) events[i].data.ptr)->Notify(events + i);
	
	return 0;
} 

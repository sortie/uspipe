#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/ioctl.h> 
#include <termios.h>
#include <errno.h>
#include <stdint.h>
#include "common.h"

int main(int argc, char* argv[])
{
	if ( argc < 3 )
	{
		fprintf(stderr, "usage: usdcl <name-path> <command> [command argument] [...]\n"); exit(1);
	}

	const char* Path = argv[1];
	const char* Command = argv[2];
	
	char** CommandArguments = new char*[argc-1];
	for ( int i = 2; i < argc; i++ )
	{
		CommandArguments[i-2] = argv[i];
	}
	CommandArguments[argc-1] = NULL;

	int ControlSocket = socket(AF_UNIX, SOCK_STREAM, 0);
	if ( ControlSocket == -1 ) { perror("socket"); exit(1);  }

	sockaddr_un ControlAddr, ClientAddr;
	ControlAddr.sun_family = AF_UNIX;
	strcpy(ControlAddr.sun_path, Path); // TODO: buffer overrun!
	unlink(ControlAddr.sun_path);

	socklen_t ControlAddrLength = strlen(ControlAddr.sun_path) + sizeof(ControlAddr.sun_family);
	if ( bind(ControlSocket, (sockaddr*) &ControlAddr, ControlAddrLength) == -1 ) { perror("bind"); exit(1); }	
	if ( listen(ControlSocket, 1) == -1 ) { perror("listen"); exit(1); }

	//int ChildPID = fork();
	//if ( ChildPID < 0 ) { perror("fork"); }
	//if ( ChildPID > 0 ) { exit(0); }

	socklen_t ClientAddrLength = sizeof(ClientAddr);
	int ClientSocket = accept(ControlSocket, (sockaddr*) &ClientAddr, &ClientAddrLength);
	if ( ClientSocket == -1 ) { perror("accept"); exit(1); }

	int InputSocket = ReceiveFileDescriptor(ClientSocket);
	if ( InputSocket < 0 ) { perror("ReceiveFileDescriptor"); exit(1); }

	int OutputSocket = ReceiveFileDescriptor(ClientSocket);
	if ( OutputSocket < 0 ) { perror("ReceiveFileDescriptor"); exit(1); }
	
	// Replace std{in,out} of the child process.
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	dup(InputSocket);
	dup(OutputSocket);

	// Clean up the now unusued file descriptors.
	close(InputSocket);
	close(OutputSocket);
	close(ClientSocket);
	close(ControlSocket);

	execvp(Command, CommandArguments);

	perror("execvp");

	return 1;

#if 0
	int InputSocket = socket(AF_UNIX, SOCK_STREAM, 0);
	if ( InputSocket == -1 ) { perror("socket"); exit(1); }
	int ControlSocket = socket(AF_UNIX, SOCK_STREAM, 0);
	if ( ControlSocket == -1 ) { perror("socket"); exit(1); }
	int OutputSocket = socket(AF_UNIX, SOCK_STREAM, 0);
	if ( OutputSocket == -1 ) { perror("socket"); exit(1); }

	sockaddr_un InputAddr, ControlAddr, ClientAddr, OutputAddr;

	InputAddr.sun_family = AF_UNIX;
	strcpy(InputAddr.sun_path, Path);
	unlink(Path);

	ControlAddr.sun_family = AF_UNIX;
	strcpy(ControlAddr.sun_path, Path); // TODO: buffer overrun!
	strcat(ControlAddr.sun_path, ".control"); // TODO: buffer overrun!
	unlink(ControlAddr.sun_path);

	socklen_t InputAddrLength = strlen(InputAddr.sun_path) + sizeof(InputAddr.sun_family);
	if ( bind(InputSocket, (sockaddr*) &InputAddr, InputAddrLength) == -1 ) { perror("bind"); exit(1); }	
	if ( listen(InputSocket, 1) == -1 ) { perror("listen"); exit(1); }

	socklen_t ControlAddrLength = strlen(ControlAddr.sun_path) + sizeof(ControlAddr.sun_family);
	if ( bind(ControlSocket, (sockaddr*) &ControlAddr, ControlAddrLength) == -1 ) { perror("bind"); exit(1); }	
	if ( listen(ControlSocket, 1) == -1 ) { perror("listen"); exit(1); }

	int ChildPID = fork();
	if ( ChildPID < 0 ) { perror("fork"); }
	if ( ChildPid > 0 ) { exit(0); }

	socklen_t ClientAddrLength = sizeof(ClientAddr);
	int ClientSocket = accept(InputSocket, (sockaddr*) &ClientAddr, &ClientAddrLength);
	if ( ClientSocket == -1 ) { perror("accept"); exit(1); }

	uint32_t OutputLength = 0;
	ReadAll(ClientSocket, &OutputLength, sizeof(OutputLength));
	// TODO: Make sure OutputLength is of an acceptable length!

	ReadAll(ClientSocket, OutputAddr.sun_path, OutputLength); // TODO: buffer overrun!
	OutputAddr.sun_path[OutputLength] = '\0'; // TODO: buffer overrun!

	socklen_t OutputAddrLength = strlen(OutputAddr.sun_path) + sizeof(OutputAddr.sun_family);
	if ( connect(OutputSocket, (sockaddr*) &OutputAddr, OutputAddrLength) != 0 ) { perror("connect"); exit(1); }

	// Replace std{in,out} of the child process with ClientSocket
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	dup(InputSocket);
	dup(OutputSocket);

	// Clean up the now unusued file descriptors.
	close(InputSocket);
	close(OutputSocket);
	close(ClientSocket);
	close(ControlSocket);

	execvp(Command, CommandArguments);

	perror("execvp");

	return 1;
#endif
}

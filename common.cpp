#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/ioctl.h> 
#include <termios.h>
#include <errno.h>
#include <stdint.h>

void ReadAll(int FileDescriptor, void* B, size_t Size)
{
	uint8_t* Buffer = (uint8_t*) B;

	while ( Size )
	{
		ssize_t Read = read(FileDescriptor, Buffer, Size);

		if ( Read < 0 ) { perror("read"); exit(1); }

		Size -= (size_t) Read;
		Buffer += Read;
	}
}

int SendFileDescriptor(int Socket, int FileDescToSend)
{
	struct msghdr Socket_message;
	struct iovec io_vector[1];
	struct cmsghdr *control_message = NULL;
	char message_buffer[1];
	/* storage space needed for an ancillary element with a paylod of length is CMSG_SPACE(length) */
	char ancillary_element_buffer[CMSG_SPACE(int)];
	int available_ancillary_element_buffer_space;

	/* at least one vector of one byte must be sent */
	message_buffer[0] = 'F';
	io_vector[0].iov_base = message_buffer;
	io_vector[0].iov_len = 1;

	/* initialize Socket message */
	memset(&Socket_message, 0, sizeof(struct msghdr));
	Socket_message.msg_iov = io_vector;
	Socket_message.msg_iovlen = 1;

	/* provide space for the ancillary data */
	available_ancillary_element_buffer_space = CMSG_SPACE(int);
	memset(ancillary_element_buffer, 0, available_ancillary_element_buffer_space);
	Socket_message.msg_control = ancillary_element_buffer;
	Socket_message.msg_controllen = available_ancillary_element_buffer_space;

	/* initialize a single ancillary data element for fd passing */
	control_message = CMSG_FIRSTHDR(&Socket_message);
	control_message->cmsg_level = SOL_SOCKET;
	control_message->cmsg_type = SCM_RIGHTS;
	control_message->cmsg_len = CMSG_LEN(sizeof(int));
	*((int *) CMSG_DATA(control_message)) = FileDescToSend;

	return sendmsg(Socket, &Socket_message, 0);
}

int ReceiveFileDescriptor(int Socket)
{
	while ( true )
	{
		char Buffer[1024];
		int BytesRead = read(0, Buffer, sizeof(Buffer));
		if ( BytesRead <= 0 ) { break; }

		int BytesWritten = write(1, Buffer, BytesRead);
		if ( BytesWritten != BytesRead ) { break; }
	}

	int sent_fd, available_ancillary_element_buffer_space;
	struct msghdr Socket_message;
	struct iovec io_vector[1];
	struct cmsghdr *control_message = NULL;
	char message_buffer[10];
	char ancillary_element_buffer[CMSG_SPACE(int)];

	/* start clean */
	memset(&Socket_message, 0, sizeof(struct msghdr));
	memset(ancillary_element_buffer, 0, CMSG_SPACE(int));

	/* setup a place to fill in message contents */
	io_vector[0].iov_base = message_buffer;
	io_vector[0].iov_len = 10;
	Socket_message.msg_iov = io_vector;
	Socket_message.msg_iovlen = 1;

	/* provide space for the ancillary data */
	Socket_message.msg_control = ancillary_element_buffer;
	Socket_message.msg_controllen = CMSG_SPACE(int);

	if(recvmsg(Socket, &Socket_message, MSG_CMSG_CLOEXEC) < 0)
	return -1;

	if(message_buffer[0] != 'F')
	{
		/* this did not originate from the above function */
		return -1;
	}

	if((Socket_message.msg_flags & MSG_CTRUNC) == MSG_CTRUNC)
	{
		/* we did not provide enough space for the ancillary element array */
		return -1;
	}

	/* iterate ancillary elements */
	for(control_message = CMSG_FIRSTHDR(&Socket_message);
	control_message != NULL;
	control_message = CMSG_NXTHDR(&Socket_message, control_message))
	{
		if( (control_message->cmsg_level == SOL_SOCKET) &&
		(control_message->cmsg_type == SCM_RIGHTS) )
		{
			sent_fd = *((int *) CMSG_DATA(control_message));
			return sent_fd;
		}
	}

	return -1;
}

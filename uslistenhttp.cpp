#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/ioctl.h> 
#include <termios.h>
#include "common.h"

#define MINIMUM(X, Y) ((X < Y) ? X : Y)

int main(int argc, char* argv[])
{
	if ( argc < 2 )
	{
		fprintf(stderr, "usage: uslistenhttp <path>\n"); exit(1);
	}

	const char* Path = argv[1];

	int Socket = socket(AF_UNIX, SOCK_STREAM, 0);
	if ( Socket == -1 ) { perror("socket"); exit(1); }
	int SocketHTTP = socket(AF_INET, SOCK_STREAM, 0);
	if ( SocketHTTP == -1 ) { perror("socket"); exit(1); }

	sockaddr_un Local, Remote;

	Local.sun_family = AF_UNIX;
	strcpy(Local.sun_path, Path);
	unlink(Path);

	socklen_t LocalLength = strlen(Local.sun_path) + sizeof(Local.sun_family);
	if ( bind(Socket, (sockaddr*) &Local, LocalLength) == -1 ) { perror("bind"); exit(1); }	
	if ( listen(Socket, 5) == -1 ) { perror("listen"); exit(1); }

	sockaddr_in LocalHTTP, RemoteHTTP;
	LocalHTTP.sin_family = AF_INET;
	LocalHTTP.sin_addr.s_addr = INADDR_ANY;
	LocalHTTP.sin_port = htons(1338);
	if ( bind(SocketHTTP, (sockaddr*) &LocalHTTP, sizeof(LocalHTTP)) == -1 ) { perror("bind"); exit(1); }	
	if ( listen(SocketHTTP, 5) == -1 ) { perror("listen"); exit(1); }

	while ( true )
	{
		socklen_t RemoteLength = sizeof(Remote);
		int ClientSocket = accept(Socket, (sockaddr*) &Remote, &RemoteLength);
		if ( ClientSocket == -1 ) { perror("accept"); exit(1); }

		bool ClientGood = true;

		while ( ClientGood )
		{
			socklen_t RemoteHTTPLength = sizeof(RemoteHTTP);
			int ClientHTTPSocket = accept(SocketHTTP, (sockaddr*) &RemoteHTTP, &RemoteHTTPLength);
			if ( ClientHTTPSocket == -1 ) { perror("accept"); exit(1); }
		
			while ( true )
			{
				fd_set ReadFSs;

				FD_ZERO(&ReadFSs);
				FD_SET(ClientSocket, &ReadFSs);
				FD_SET(ClientHTTPSocket, &ReadFSs);

				if ( select(sizeof(ReadFSs)*8, &ReadFSs, NULL, NULL, NULL) == -1 ) { break; }

				char Buffer[1024];
				int Dest, Source;

				if ( FD_ISSET(ClientSocket, &ReadFSs) )
				{
					uint32_t Left;
					ReadAll(ClientSocket, &Left, sizeof(Left));

					if ( Left == 0 ) { break; }

					printf("reading %u bytes!\n", Left);

					while ( Left > 0 )
					{			
						printf("-- %u bytes left!\n", Left);
		
						int BytesRead = read(ClientSocket, Buffer, MINIMUM(Left, sizeof(Buffer)));
						if ( BytesRead <= 0 ) { ClientGood = false; break; }

						Left -= BytesRead;

						write(1, Buffer, BytesRead);

						int BytesWritten = write(ClientHTTPSocket, Buffer, BytesRead);
						if ( BytesWritten != BytesRead )
						{
							Left = 0;
							BytesWritten = write(ClientSocket, &Left, sizeof(Left));
							if ( BytesWritten <= 0 ) { ClientGood = false; break; }
							break;
						}
					}

					printf("done!\n", Left);
				}
				else
				{			
					int BytesRead = read(ClientHTTPSocket, Buffer, sizeof(Buffer));
					if ( BytesRead <= 0 )
					{
						uint32_t Left = 0;
						int BytesWritten = write(ClientSocket, &Left, sizeof(Left));
						if ( BytesWritten <= 0 ) { ClientGood = false; }
						break;
					}

					write(1, Buffer, BytesRead);

					uint32_t Left = BytesRead;
					int BytesWritten = write(ClientSocket, &Left, sizeof(Left));
					if ( BytesWritten <= 0 ) { ClientGood = false; break; }

					BytesWritten = write(ClientSocket, Buffer, BytesRead);
					if ( BytesWritten <= 0 ) { ClientGood = false; break; }
				}
			}

			close(ClientHTTPSocket);
		}

		close(ClientSocket);
	}

	return 0;
}

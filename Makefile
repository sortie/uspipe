CC=c++
CPPFLAGS=-g3
BINARIES=usdcl uslisten uspipe uslistenhttp

all: $(BINARIES)

usdcl: usdcl.o common.o

uslisten: uslisten.o common.o

uslistenhttp: uslistenhttp.o common.o

uspipe: uspipe.o common.o

clean:
	rm -rf $(BINARIES) *.o

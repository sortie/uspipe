#ifndef US_COMMON_H
#define US_COMMON_H
void ReadAll(int FileDescriptor, void* B, size_t Size);
int SendFileDescriptor(int Socket, int FileDescToSend);
int ReceiveFileDescriptor(int Socket);
#endif

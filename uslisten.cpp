#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/ioctl.h> 
#include <termios.h>

int main(int argc, char* argv[])
{
	if ( argc < 2 )
	{
		fprintf(stderr, "usage: uslisten <path>\n"); exit(1);
	}

	struct termios TermMode;

	ioctl(STDIN_FILENO, TCGETS, &TermMode); // Get current terminal mode	
	TermMode.c_lflag &= ~ECHO; // Prevent the terminal from echoing our input
	TermMode.c_lflag &= ~ICANON; // Prevent the terminal from buffering the lines
	TermMode.c_lflag &= ~ISIG; // Disable signals
	ioctl(STDIN_FILENO, TCSETS, &TermMode); // Set new terminal mode

	const char* Path = argv[1];

	int Socket = socket(AF_UNIX, SOCK_STREAM, 0);
	if ( Socket == -1 ) { perror("socket"); exit(1); }

	sockaddr_un Local, Remote;

	Local.sun_family = AF_UNIX;
	strcpy(Local.sun_path, Path);
	unlink(Path);

	socklen_t LocalLength = strlen(Local.sun_path) + sizeof(Local.sun_family);
	if ( bind(Socket, (sockaddr*) &Local, LocalLength) == -1 ) { perror("bind"); exit(1); }	
	if ( listen(Socket, 5) == -1 ) { perror("listen"); exit(1); }

	while ( true )
	{
		socklen_t RemoteLength = sizeof(Remote);
		int ClientSocket = accept(Socket, (sockaddr*) &Remote, &RemoteLength);
		if ( ClientSocket == -1 ) { perror("accept"); exit(1); }

		while ( true )
		{
			fd_set ReadFSs;

			FD_ZERO(&ReadFSs);
			FD_SET(ClientSocket, &ReadFSs);
			FD_SET(STDIN_FILENO, &ReadFSs);

			if ( select(sizeof(ReadFSs)*8, &ReadFSs, NULL, NULL, NULL) == -1 ) { break; }

			int Dest, Source;

			if ( FD_ISSET(STDIN_FILENO, &ReadFSs) )
			{
				Dest = ClientSocket; Source = STDIN_FILENO;
			}
			else
			{
				Dest = STDOUT_FILENO; Source = ClientSocket;
			}

			char Buffer[1024];
			int BytesRead = read(Source, Buffer, sizeof(Buffer));
			if ( BytesRead <= 0 ) { break; }

			int BytesWritten = write(Dest, Buffer, BytesRead);
			if ( BytesWritten != BytesRead ) { break; }
		}

		close(ClientSocket);
	}

	return 0;
}

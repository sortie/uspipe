#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/ioctl.h> 
#include <termios.h>
#include <errno.h>
#include <stdint.h>
#include "common.h"

int main(int argc, char* argv[])
{
	if ( argc < 2 )
	{
		fprintf(stderr, "usage: uspipe <name-path> [...]\n"); exit(1);
	}

	size_t NumElems = argc-1;
	int* Handles = new int[NumElems+2];

	for ( size_t I = 1; I <= NumElems; I++ )
	{
		const char* Path = argv[I+1];

		int ChildSocket = socket(AF_UNIX, SOCK_STREAM, 0);
		if ( ChildSocket == -1 ) { perror("socket"); exit(1); }

		sockaddr_un ChildAddr;
		ChildAddr.sun_family = AF_UNIX;
		strcpy(ChildAddr.sun_path, argv[I]); // TODO: buffer overrun!

		socklen_t ChildAddrLength = strlen(ChildAddr.sun_path) + sizeof(ChildAddr.sun_family);
		if ( connect(ChildSocket, (sockaddr*) &ChildAddr, ChildAddrLength) != 0 ) { perror("connect"); exit(1); }

		Handles[I] = ChildSocket;	
	}

	Handles[0] = STDIN_FILENO;
	Handles[NumElems+1] = STDOUT_FILENO;

	for ( size_t I = 1; I <= NumElems; I++ )
	{
		SendFileDescriptor(Handles[I], Handles[I-1]);
	}

	for ( size_t I = NumElems; I > 0; I-- )
	{
		SendFileDescriptor(Handles[I], Handles[I+1]);
	}

	delete[] Handles;

	return 0;
}
